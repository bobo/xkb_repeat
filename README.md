This project collects patches to modify X server repeat key behavior. You could apply the patch and recompile your X server.
Please find the patches following this link: https://git.framasoft.org/bobo/xkb_repeat/tree/master/patches

Why would you use those patches ?
=================================
If you play Starcraft 2, there is funny business to do with the keyboard. Some guys created alternative ergonomic keybindings, within TheCore project, as an example. Most of those works make use of "rapid fire hotkey". The idea behind this term is to keep a key pressed for an action to repeat, avoiding any useless click. Some more information about rapid fire keys could be find here at http://www.teamliquid.net/forum/sc2-strategy/446530-rapid-fire-hotkey-trick

Some rapid fire tips are using a Window only bug for some keyboard only in a really creative way:
* https://www.youtube.com/watch?v=pIbo94IWeu8 rapid fire inject (no longer valid for LoTV)
* https://www.youtube.com/watch?v=N2b-C6X21nk rapid fire worker split

This is only possible with a keyboard able to output the following when 2 keys are simultaneously pressed:
```
jkjkjkjkjkjkjkjkjkjkjkjkjkjkjkjkjkjkjkjkjkjkjkjkjkjkjkjkjkjkjkjkjkjkjkjkjkjkjk
```
Non-eligible keyboards output is:
```
jkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk
```
It appears that even with the right keyboard, this behavior never happens on Linux. This is a Windows-only thing due to some keyboard driver bug. This could be quite frustrating for Linux based OS users.

On Linux based OS, our keyboard driver is freesoftware (libre), generic and part of the X server.
As a consequence we are free to adapt it to fit our needs.
This page collects all patches changing the repeat behavior of the X server.
If we succeed in federating a sort of community, we could think about creating the ultimate patch, generic adaptable with xset command, and submit it for inclusion in the upstream project.

Possible contributions
======================
* There is no package at the moment. If somebody is interested to get one for Archlinux, please ask, I could deal with it. If somebody created a package for another distro, please ping me so that this page could point at it
* In case you enhanced a patch or created another one, please note that this project is opened to contributors.
If you want to experiment, I recommand to start with the xkb-repeat2-easy.patch, which is the minimal modification approach I found to emulate the desired behavior.
* Install procedure for others X server based OS are also welcome, to help others to experiment
* Feel free to communicate any other idea

How to recompile xorg-server to support alternative repeat style
================================================================
The code for those steps is for Archlinux only. For other OS, you would have to adapt them. It targets xkb-repeat2-easy.patch, of course you could adapt to apply any other patch.

1. get the source
```
yaourt -G xorg-server
cd xorg-server
```

2. get the patch code
```
wget https://git.framasoft.org/bobo/xkb_repeat/raw/master/patches/xkb-repeat2-easy.patch
```

3. prepare the source
```
makepkg -s --skippgpcheck -o
```

4. apply patch
```
cd src/xorg-server-$pkgver
patch -Np1 -i ../../xkb-repeat2-easy.patch
cd ../..
```

5. compile
```
makepkg -e -f --noprepare
```

6. install
```
yaourt -U xorg-server-$pkgver-$pkgrel-$arch.pkg.tar.xz
```
