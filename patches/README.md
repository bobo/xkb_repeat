Patch details
=============

xkb-repeat2-easy.patch
----------------------
This patch enables a simple 2 keys repeat.
It is easy to use, with a quite flexible in the time window to activate the double-key repeat.
The behaviour is consistent.
The repeat last until the first pressed key is released.
If you wanted to hack a bit, this is probably the best approach to look at first to understand xkb repeat approach.

xkb-repeat2-winlike.patch
-------------------------
This patch enables a windows-like 2 keys repeat.
Same behavior as xkb-repeat2-easy.patch except a short time window to activate the double key repeat.
You could compare it to xkb-repeat2-easy.patch to see the similarities between them. 

xkb-repeat3-easy.patch
----------------------
This patch is an enhancement of xkb-repeat2-easy.patch to enable 3 keys repeat.
It uses the same principle and creates some more complication.
The initial purpose was to re-enable the Rapid Fire Inject for Legacy of The Void.

There is a behaviour inconsistency that make it not so "easy" to use:
it is more difficult to make the 3 keys output consistently for the first pattern.
As a example pressing J, K and L keys could produces those results depending on the timing:
```
jkljkljkljkljkljkljkljkljkljkljkljkljkljkljkljkl
jkjkljkljkljkljkljkljkljkljkljkljkljkljkljkljkljkl
```

There are also another ununderstood side effect:
you could sometimes get the impression that a keystroke is not taken into account. 
If you succeeded in troubleshooting&fix it, I would be happy to get informed :)

xkb-repeat-pattern.patch
------------------------

N-keys repeat solution based on a 16 keycode table.
Once the repeat pattern is full, a new timer is launched so that a maxPatternLen=1 would keep the behavior as current.
The last pressed key become the repeatKey, if it is released the repeat ends.

The variable "XkbDflMaxPatternLen" could be adjusted from 1 to 16 depending on your preference.
A patch could be submitted to Xorg with XkbDflMaxPatternLen=1,
and a solution based on/similar to "xset r rate" to override the default behavior
(just like "XkbDfltRepeatDelay" and "XkbDfltRepeatInterval" can be overrode).

Futures patches to be released
==============================
* xkb-repeat3-pattern.patch (another approach with a more generic/scalable repeat pattern)

How to make a patch
===================
```
cd modified-xorg-server-source-dir 
diff -rup original-xorg-server-source-dir . | grep -v "^Only" > patch_name.patch
```

License
=======
Those patches are provided under the license term encouraged by X.org foundation
